from scipy.optimize import Bounds
from scipy.optimize import BFGS
from scipy.optimize import LinearConstraint
from scipy.optimize import NonlinearConstraint
from scipy.optimize import minimize
import numpy as np

emission_dic ={'opc' : 0.85, # in ton of CO2
          'fly_ash' : 0.004,# values from Alsalaman + al
          'silica_fume' : 0.014, 
          'GGBFS' : 0.052,
          'metakolain' : 0.33,
          'fine_aggregate' :0.0048,
          'coarse_aggregate':0.0048,
          'admixture': 1.88,
          'sodium_hydroxide': 1.915,
          'sodium_silicate': 1.222,
          'limestone': 0.00313,  #value from A Review of Carbon Footprint Reduction in Construction Industry, from Design to Operation  Sizirici + al
          'calcined_clay': 0.196, #best value from Limestone calcined clay cement as a low-carbon solution to meet expanding cement demand in emerging economies, Yudiesky + al.
          'pozzolan': 0.00313, #value not found (so same as limestone as there are both existing in natural state
}

def co2_emission_fct(recipe_dic):
    ''' Function to compute the CO2 emission of a given recipe'''
    co2_emission = 100*emission_dic['opc']
    material_mass = 100
    for i in ['limestone', 'silica_fume', 'GGBFS', 'fly_ash', 'calcined_clay']:
        switch={'limestone': recipe_dic[-5],
                'silica_fume':recipe_dic[-4],
                'GGBFS':recipe_dic[-3],
                'fly_ash':recipe_dic[-2],
                'calcined_clay': recipe_dic[-1],
                }
        mass = switch.get(i)
        co2_emission += emission_dic[i]* mass
        material_mass += mass
    return co2_emission/material_mass

def opt(b_b_function, c_function, x, initial_point = None):
    ''' Standard optimization function '''
    def fun(x):
        return co2_emission_fct(x)
    def con(x):
        return c_function.predict(x.reshape(-1,1).transpose(), verbose = 0)[0][1]
    print('Initialization of the constraints')
    bounds = Bounds([45, 0, 4, 7, 1, 25, 385, 0.3, 1, 0, 0, 0, 0, 0] , [80, 32, 14, 15, 1, 25, 385, 2, 5, 15, 20, 25, 16, 45])
    m = [[1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    v = [100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    linear_constraint = LinearConstraint(m,v,v)
    nonlinear_constraint = NonlinearConstraint(con, 10, np.inf, jac='2-point', hess=BFGS())
    min_dic = {}
    for i in range(x.shape[0]):
        print(f'Begining of the {i}th iteration') 
        x1 = x.to_numpy()
        x0 = x1[i,:]
        res = minimize(fun, x0, method='trust-constr', jac='2-point', hess=BFGS(),
               constraints=[linear_constraint, nonlinear_constraint],
               options={'verbose': 0}, bounds=bounds, tol = 1e-5)
        min_dic[f'Iteration {i}'] = {'recipe': res.x, 'co2_value': res.fun}
        print(f'Result for the {i}th iteration: {res.x} with CO2 emissions of {res.fun}' )
    return min_dic

def opt_gp(b_b_function, c_function, x, initial_point = None):
    ''' Standard optimization function '''
    def fun(x):
        return co2_emission_fct(x)
    def con(x):
        return c_function.predict(x.reshape(1, -1))[0][1]
    print('Initialization of the constraints')
    bounds = Bounds([45, 0, 4, 7, 1, 25, 385, 0.3, 1, 0, 0, 0, 0, 0] , [80, 32, 14, 15, 1, 25, 385, 2, 5, 15, 20, 25, 16, 45])
    m = [[1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    v = [100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    linear_constraint = LinearConstraint(m,v,v)
    nonlinear_constraint = NonlinearConstraint(con, 10, np.inf, jac='2-point', hess=BFGS())
    min_dic = {}
    for i in range(x.shape[0]):
        print(f'Begining of the {i}th iteration') 
        x1 = x.to_numpy()
        x0 = x1[i,:]
        res = minimize(fun, x0, method='trust-constr', jac='2-point', hess=BFGS(),
               constraints=[linear_constraint, nonlinear_constraint],
               options={'verbose': 0}, bounds=bounds, tol = 1e-5)
        min_dic[f'Iteration {i}'] = {'recipe': res.x, 'co2_value': res.fun}
        print(f'Result for the {i}th iteration: {res.x} with CO2 emissions of {res.fun}' )
    return min_dic

def opt_gp1(b_b_function, c_function, x, initial_point = None):
    ''' Standard optimization function '''
    def fun(x):
        return co2_emission_fct(x)
    def con(x):
        return c_function.predict(x.reshape(1, -1))[0][1]
    print('Initialization of the constraints')
    bounds = Bounds([45, 0, 4, 7, 1, 25, 385, 0.3, 1, 0, 0, 0, 0, 0] , [80, 32, 14, 15, 1, 25, 385, 2, 5, 30, 30, 80, 40, 70])
    m = [[1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    v = [100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    linear_constraint = LinearConstraint(m,v,v)
    nonlinear_constraint = NonlinearConstraint(con, 10, np.inf, jac='2-point', hess=BFGS())
    min_dic = {}
    for i in range(x.shape[0]):
        print(f'Begining of the {i}th iteration') 
        x1 = x.to_numpy()
        x0 = x1[i,:]
        res = minimize(fun, x0, method='trust-constr', jac='2-point', hess=BFGS(),
               constraints=[linear_constraint, nonlinear_constraint],
               options={'verbose': 0}, bounds=bounds, tol = 1e-2)
        min_dic[f'Iteration {i}'] = {'recipe': res.x, 'co2_value': res.fun}
        print(f'Result for the {i}th iteration: {res.x} with CO2 emissions of {res.fun}' )
    return min_dic



def opt_ismo(c_function, initial_point):
    ''' Standard optimization function '''
    def fun(x):
        result = co2_emission_fct(x)
        return result
    def con(x):
        return c_function.predict(x.reshape(-1,1).transpose(), verbose = 0)[0][1]
    print('Initialization of the constraints')
    bounds = Bounds([45, 0, 4, 7, 1, 25, 385, 0.3, 1, 0, 0, 0, 0, 0] , [80, 32, 14, 15, 1, 25, 385, 2, 5, 15, 20, 25, 16, 45])
    m = [[1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    v = [100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    linear_constraint = LinearConstraint(m,v,v)
    nonlinear_constraint = NonlinearConstraint(con, 10, np.inf, jac='2-point', hess=BFGS())
    #if initial_point.any() != None:
    x0 = initial_point
    print('Begin Minimizing Algorithm')
    res = minimize(fun, x0, method='trust-constr', jac='2-point', hess=BFGS(),
               constraints=[linear_constraint, nonlinear_constraint],
               options={'verbose': 0}, bounds=bounds, tol = 1e-5)
    print(f'Result: {res.x} with CO2 emissions of {res.fun}' )
    #print(res.x)
    return res.x, res.fun