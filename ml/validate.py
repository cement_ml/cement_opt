import tensorflow as tf
import pandas as pd
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error, r2_score
import os

import matplotlib.pyplot as plt
import seaborn as sns


def evaluate_predictions(true_values, predicted_values):
    '''Function to compute the relative error, the mean square error and the R^2 of the model for each output and for the mean
       over all the output'''
    # Initialize lists to store results
    relative_errors = []
    mean_squared_errors = []
    coefficients_of_determination = []
    true_values = true_values.values
    # if predicted_values as only 1 dim reshape it
    if predicted_values.shape == (predicted_values.shape[0],):
        predicted_values = predicted_values.reshape(-1,1)
    # Loop through each output dimension
    for i in range(true_values.shape[1]):
        if True:# i not in [6,7]
            true_vals_dim = true_values[:, i]
            pred_vals_dim = predicted_values[:, i]

            # Calculate relative error
            relative_error = abs(true_vals_dim - pred_vals_dim) / true_vals_dim
            relative_errors.append(relative_error.mean())

            # Calculate mean squared error
            mse = mean_squared_error(true_vals_dim, pred_vals_dim)
            mean_squared_errors.append(mse)

            # Calculate coefficient of determination (R-squared)
            r_squared = r2_score(true_vals_dim, pred_vals_dim)
            coefficients_of_determination.append(r_squared)

        # Calculate mean values across dimensions
        mean_relative_error = sum(relative_errors) / len(relative_errors)
        mean_mse = sum(mean_squared_errors) / len(mean_squared_errors)
        mean_r_squared = sum(coefficients_of_determination) / len(coefficients_of_determination)

        # Create a pandas DataFrame to store results
        results = pd.DataFrame({
            'RelativeError': relative_errors,
            'MeanSquaredError': mean_squared_errors,
            'CoefficientOfDetermination': coefficients_of_determination
        })
        # Add row for mean values across dimensions
        mean_values_row = pd.Series({
            'Relative Error mean': mean_relative_error,
            'Mean Squared Error mean': mean_mse,
            'Coefficient of Determination mean': mean_r_squared
        }, name='Mean across Dimensions')

        # Append the mean values row to the results DataFrame
     #   results = results.append(mean_values_row)
       # results = pd.concat([results, mean_values_row])
    return results



def plot_true_vs_predicted(true_values, predicted_values, name: str):
    '''Function to plot the true values against the predicted one from the model and save the result under name_predict_vs_true'''
    names = true_values.columns.tolist()
    true_values = true_values.values
    num_dimensions = true_values.shape[1]

    # Calculate the number of rows and columns for subplots
    num_rows = (num_dimensions + 1) // 2
    num_cols = 2 if num_dimensions > 1 else 1

    # Create a figure with subplots for each dimension
    fig, axes = plt.subplots(nrows=num_rows, ncols=num_cols, figsize=(10, 5 * num_rows))

    # Iterate through each dimension
    for i in range(num_dimensions):
        row = i // 2
        col = i % 2
        if num_dimensions % 2 == 1 and i == num_dimensions - 1:
            # If there's an odd number of dimensions, and it's the last one, center it
            ax = axes[row, 0] if num_dimensions > 1 else axes
        else:
            ax = axes[row, col] if num_dimensions > 1 else axes

        # Scatter plot of true vs predicted values for the current dimension
        sns.scatterplot(x=true_values[:, i], y=predicted_values[:, i], ax=ax)

        # Plotting the line x=y
        ax.plot(true_values[:, i], true_values[:, i], color='red', linestyle='--')
        
        name_plot = names[i]
        # Set labels and title for the subplot
        ax.set_xlabel(f'True Values for {name_plot}')
        ax.set_ylabel(f'Predicted Values using {name}')
        ax.set_title(f'True vs Predicted for {name_plot}')

    plt.tight_layout()

    save_folder = 'plots'
    plot_filename = os.path.join(save_folder, f'{name}_predict_vs_true.png')
    plt.savefig(plot_filename)
    plt.show()


def plot_model1_vs_model2(true_values, predicted_values, name):
    '''Function to plot the predicted values from model 1 against the predicted values from model 2 and save the result under name_predict_vs_true'''
    num_dimensions = true_values.shape[1]
    # Create a figure with subplots for each dimension
    fig, axes = plt.subplots(nrows=2, ncols=num_dimensions // 2, figsize=(5 * num_dimensions, 10))
    
    name_dic = {0: 'K', 1: 'G', 2: 'E', 3: 'nu'}
    
    # Calculate the number of rows and columns for subplots
    num_rows = (num_dimensions + 1) // 2
    num_cols = 2 if num_dimensions > 1 else 1

    # Iterate through each dimension
    for i in range(num_dimensions):
        row = i // 2
        col = i % 2
        if num_dimensions % 2 == 1 and i == num_dimensions - 1:
            # If there's an odd number of dimensions, and it's the last one, center it
            ax = axes[row, 0] if num_dimensions > 1 else axes
        else:
            ax = axes[row, col] if num_dimensions > 1 else axes

        # Scatter plot of true vs predicted values for the current dimension
        sns.scatterplot(x=true_values[:, i], y=predicted_values[:, i], ax=ax)

        # Plotting the line x=y
        ax.plot(true_values[:, i], true_values[:, i], color='red')

        # Set labels and title for the subplot
        ax.set_xlabel(f'Direct Elastic Modulus Values')
        ax.set_ylabel(f'Elastic Modulus via Hydration Product Values')
        ax.set_title(f'{name_dic[i]}')

    plt.tight_layout()
    
    save_folder = 'plots'
    plot_filename = os.path.join(save_folder, f'{name}_predict_vs_true.png')
    plt.savefig(plot_filename)
    plt.show()

    
def plot_loss(model, name : str, nb_data_points : int):
    '''Function to plot the loss graphic'''
    save_folder = 'plots'

    plt.plot(model.history['loss'][1:])
    plt.title(f'{name} NN Loss for {nb_data_points} data points')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')

    plot_filename = os.path.join(save_folder, f'{name}_nn_loss_plot_{nb_data_points}.png')
    plt.savefig(plot_filename)

    plt.show()
