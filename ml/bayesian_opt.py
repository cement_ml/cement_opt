import sys
sys.path.append( '/digital_twin/prop/' )
sys.path.append( '/digital_twin/util/' )

from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from scipy.optimize import NonlinearConstraint
from scipy.optimize import LinearConstraint
from util.final_hydration import final_hydration
from prop.energy import energy_emission_data
from prop.elastic_moduli import elastic_moduli_1d
from varname import nameof
import numpy as np
import pandas as pd



emission_dic ={'opc' : 0.85, # in ton of CO2
          'fly_ash' : 0.004,# values from Alsalaman + al
          'silica_fume' : 0.014, 
          'GGBFS' : 0.052,
          'metakolain' : 0.33,
          'fine_aggregate' :0.0048,
          'coarse_aggregate':0.0048,
          'admixture': 1.88,
          'sodium_hydroxide': 1.915,
          'sodium_silicate': 1.222,
          'limestone': 0.00313,  #value from A Review of Carbon Footprint Reduction in Construction Industry, from Design to Operation  Sizirici + al
          'calcined_clay': 0.196, #best value from Limestone calcined clay cement as a low-carbon solution to meet expanding cement demand in emerging economies, Yudiesky + al.
          'pozzolan': 0.00313, #value not found (so same as limestone as there are both existing in natural state
}

def BO(b_b_function, c_function, inp):
    
    def hydration(C3S,C2S,C3A,C4AF,wc,CSH2,limestone,silica_fume,GGBFS,fly_ash,calcined_clay):
        output_materials = ['aq_gen', 'CSHQ', 'Portlandite', 'C3AH6', 'ettringite-AlFe', 'C4AsH12']
        recipe = {'clink_phases': {'C3S': C3S, 'C2S': C2S, 'C3A': C3A, 'C4AF': C4AF},
                  'RH': 1,
                  'T': 25,
                  'fineness': 385,
                  'wc': wc,
                  'CSH2': CSH2,
                  'limestone':limestone,
                  'silica_fume': silica_fume,
                  'GGBFS': GGBFS,
                  'fly_ash': fly_ash,
                  'calcined_clay': calcined_clay,
                  'wc_ratio' : None
                 }
        x = final_hydration(recipe, fail = True)
        
        x = pd.Series(x)
        other_material = x.loc[~x.index.isin(output_materials)].sum() #sum of all materials that are not of interest
        #print(x.index)
        x = x.loc[output_materials] #material of interest
        x['other_material'] = other_material
        return x
    
    def black_box_function(C3S,C2S,C3A,C4AF,wc,CSH2,limestone,silica_fume,GGBFS,fly_ash,calcined_clay):
        """Function with unknown in yternals we wish to maximize."""
        co2_emission = 100*emission_dic['opc']
        for i in ['limestone', 'silica_fume', 'GGBFS', 'fly_ash', 'calcined_clay']:
            switch={'limestone':limestone,
                    'silica_fume':silica_fume,
                    'GGBFS':GGBFS,
                    'fly_ash':fly_ash,
                    'calcined_clay': calcined_clay
                    }
            n = switch.get(i)
            co2_emission += emission_dic[i]* n
        return -co2_emission

    # define the constraint we want 
    def constraint_function(C3S,C2S,C3A,C4AF,wc,CSH2,limestone,silica_fume,GGBFS,fly_ash,calcined_clay):
        x = hydration(C3S,C2S,C3A,C4AF,wc,CSH2,limestone,silica_fume,GGBFS,fly_ash,calcined_clay)
        elastic_moduli = elastic_moduli_1d(x)
        return np.array([elastic_moduli, C3S + C2S + C3A + C4AF + CSH2])
    
    lower_limit = [10, 99.9]
    upper_limit =[np.inf, 100.1]
    nonlinear_constraint = NonlinearConstraint(constraint_function, lower_limit, upper_limit)
    pbounds = {"C3S":(45,80),
               "C2S": (0,32),
               "C3A":(4,14),
               "C4AF":(7,15),
               'wc':(0.5,20), 
               'CSH2': (1,15),
               'limestone': (0,15), 
               'silica_fume': (0,20),
               'GGBFS': (0,25),
               'fly_ash':(0,16),
               'calcined_clay': (0,45)
              }

    # saving the model
    logger = JSONLogger(path="./logs.log")

    optimizer = BayesianOptimization(
    f=black_box_function,
    pbounds=pbounds,
    constraint=nonlinear_constraint,
    verbose=2, # verbose = 1 prints only when a maximum is observed, verbose = 0 is silent
    random_state=1,
    )
    
    optimizer.maximize(
    init_points=2,
    n_iter=10,
    )
    
    # save the model
    optimizer.subscribe(Events.OPTIMIZATION_STEP, logger)

    for i, res in enumerate(optimizer.res):
        print("Iteration {}: \n\t{}".format(i, res))


