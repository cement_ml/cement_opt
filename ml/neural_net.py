import tensorflow as tf
import pandas as pd
import numpy as np
from tensorboard.plugins.hparams import api as hp
tf.keras.utils.set_random_seed(0)
import pandas as pd
from sklearn.metrics import mean_squared_error, r2_score
import os

import matplotlib.pyplot as plt
import seaborn as sns



def NN(train_input, train_output, test_input, test_output):
    ''' Function that intentiate a neural network'''
    # define input-output dimension
    INPUT_DIM = train_input.columns.shape[0]
    OUTPUT_DIM = train_output.columns.shape[0]
    
    # define the architecture of the network
    print('Constructing the network')
    model_layers = [tf.keras.layers.InputLayer(input_shape=(INPUT_DIM,))]
    for i in range(10):
        model_layers.append(tf.keras.layers.Dense(50, activation='relu'))

    model_layers.append(tf.keras.layers.Dense(OUTPUT_DIM))
    model = tf.keras.models.Sequential(model_layers)   

    # print the model summary
    model.summary()
    
    # train the network
    print('\n')
    print('start training the network')
    X = tf.convert_to_tensor(train_input, dtype = tf.float32)
    y = tf.convert_to_tensor(train_output, dtype = tf.float32)
    model.compile(loss=tf.keras.losses.MeanSquaredError(),
                optimizer=tf.keras.optimizers.Adam(),
                 metrics = [tf.keras.losses.MeanSquaredError()])
    MAX_EPOCHS = 100
    
    # save the history for the loss graph
    hist = model.fit(X, y, epochs=MAX_EPOCHS, validation_split = 0.1, verbose = 0)
    
    # validate the neural network with test data
    X_test = tf.convert_to_tensor(test_input, dtype = tf.float32)
    y_test = tf.convert_to_tensor(test_output, dtype = tf.float32)
    test_loss, test_acc = model.evaluate(X_test,  y_test)
    print('Loss on test set ', test_loss, 'accuracy on test_set ', test_acc)
    return model, hist

def NN_co2(train_input, train_output, test_input, test_output):
    ''' Function that intentiate a neural network to compute CO2 emissions'''
    #set input and output dimension
    INPUT_DIM = train_input.columns.shape[0]
    OUTPUT_DIM = train_output.columns.shape[0]

    # construct the neural network
    print('Constructing the network')
    model_layers = [tf.keras.layers.InputLayer(input_shape=(INPUT_DIM,))]
    for i in range(3):
        model_layers.append(tf.keras.layers.Dense(8, activation='relu'))

    model_layers.append(tf.keras.layers.Dense(OUTPUT_DIM))
    model = tf.keras.models.Sequential(model_layers)   
    
    # print the summary of the network architecture
    model.summary()
    
    # train the model
    print('\n')
    print('start training the network')
    X = tf.convert_to_tensor(train_input, dtype = tf.float32)
    y = tf.convert_to_tensor(train_output, dtype = tf.float32)
    model.compile(loss=tf.keras.losses.MeanSquaredError(),
                optimizer=tf.keras.optimizers.Adam(),
                 metrics = [tf.keras.losses.MeanSquaredError()])
    MAX_EPOCHS = 100
    
    # test the model
    hist = model.fit(X, y, epochs=MAX_EPOCHS, validation_split = 0.1, verbose = 0)
    X_test = tf.convert_to_tensor(test_input, dtype = tf.float32)
    y_test = tf.convert_to_tensor(test_output, dtype = tf.float32)
    test_loss, test_acc = model.evaluate(X_test,  y_test)
    print('Loss on test set ', test_loss, 'accuracy on test_set ', test_acc)
    return model, hist


def hyp_param_opt(x_train, y_train, x_test, y_test):
    ''' Function to do hyperparameter tuning of the neural network
        Code adapted from 
        https://www.tensorflow.org/tensorboard/hyperparameter_tuning_with_hparams
        and
        https://stackoverflow.com/questions/68702295/tensorflow-tensorboard-hparams '''
    INPUT_DIM = x_train.columns.shape[0]
    OUTPUT_DIM = y_train.columns.shape[0]
    
    HP_NUM_UNITS_1 = hp.HParam('num_units_1', hp.Discrete([8, 10]))
    HP_NUM_UNITS_2 = hp.HParam('num_units_2', hp.Discrete([8,10]))
    HP_NUM_UNITS_3 = hp.HParam('num_units_3', hp.Discrete([8, 10]))
    HP_ACTIVATION = hp.HParam('activation', hp.Discrete(['relu', 'tanh']))
    HP_DROPOUT = hp.HParam('dropout', hp.RealInterval(0.1, 0.2))
    HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(['adam', 'sgd', 'rmsprop']))
    HP_LR = hp.HParam('lr',hp.RealInterval(1e-4, 0.05))

    METRIC_ACCURACY = 'accuracy'

    with tf.summary.create_file_writer('logs/hparam_tuning').as_default():
        hp.hparams_config(
        hparams=[HP_NUM_UNITS_1, HP_NUM_UNITS_2, HP_NUM_UNITS_3, HP_ACTIVATION, HP_DROPOUT, HP_OPTIMIZER, HP_LR],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
      )
    def train_test_model(hparams):
        model = tf.keras.models.Sequential([
        tf.keras.layers.InputLayer(input_shape=(INPUT_DIM,)),
        tf.keras.layers.Dense(hparams[HP_NUM_UNITS_1], activation=hparams[HP_ACTIVATION]),
        tf.keras.layers.Dense(hparams[HP_NUM_UNITS_2], activation=hparams[HP_ACTIVATION]),
        tf.keras.layers.Dense(hparams[HP_NUM_UNITS_3], activation=hparams[HP_ACTIVATION]),
        tf.keras.layers.Dropout(hparams[HP_DROPOUT]),
        tf.keras.layers.Dense(OUTPUT_DIM)
          ])
        if hparams[HP_OPTIMIZER] == "SGD":
            optimizer = tf.keras.optimizers.SGD(learning_rate=float(hparams[HP_LR]))
        elif hparams[HP_OPTIMIZER] == "adam":
            optimizer = tf.keras.optimizers.Adam(learning_rate=float(hparams[HP_LR]))
        else:
            optimizer = tf.keras.optimizers.RMSprop(learning_rate=float(hparams[HP_LR]))
        model.compile(
          optimizer=optimizer,
          loss='mean_squared_error',
          metrics=['accuracy'],
          )

        model.fit(x_train, y_train, epochs=100, verbose = 0) 
        loss, accuracy = model.evaluate(x_test, y_test)
        return loss
    def run(run_dir, hparams):
        with tf.summary.create_file_writer(run_dir).as_default():
            hp.hparams(hparams)  # record the values used in this trial
            loss = train_test_model(hparams)
            tf.summary.scalar(METRIC_ACCURACY, loss, step=100)

    session_num = 0

    for num_units_1 in HP_NUM_UNITS_1.domain.values:
        for num_units_2 in HP_NUM_UNITS_2.domain.values:
            for num_units_3 in HP_NUM_UNITS_3.domain.values:
                for activation in HP_ACTIVATION.domain.values:
                    for lr in (HP_LR.domain.min_value, HP_LR.domain.max_value):
                        for dropout_rate in (HP_DROPOUT.domain.min_value, HP_DROPOUT.domain.max_value):
                            for optimizer in HP_OPTIMIZER.domain.values:
                                hparams = {
                                    HP_NUM_UNITS_1: num_units_1,
                                    HP_NUM_UNITS_2: num_units_2,
                                    HP_NUM_UNITS_3: num_units_3,
                                    HP_ACTIVATION: activation,
                                    HP_LR : lr,
                                    HP_DROPOUT: dropout_rate,
                                    HP_OPTIMIZER: optimizer,
                                    }
                                run_name = "run-%d" % session_num
                                print('--- Starting trial: %s' % run_name)
                                print({h.name: hparams[h] for h in hparams})
                                run('logs/hparam_tuning/' + run_name, hparams)
                                session_num += 1


