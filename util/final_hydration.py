from run.GEMSCalc import GEMS
from run.hydration import parrot_killoh
import pandas as pd
import numpy as np

# dictionary for the recipe of some SCM from CEMGEMS CEM-II-BV based on 100 g of each SCM  always give formula in moles!!!
SCM = { 'limestone': ({'Al' : 0.052961280988, 'C' : 0.85663128045, 'Ca': 0.75431457236,'Fe' : 0.025048813876, 'K': 0.012739394454,'Mg' : 0.044660136362, 'Na': 0.016134497168, 'O': 3.0564427725, 'Si':0.20637670739},  'mol'),
        'silica_fume':({'Al' : 0.005883425509, 'Ca': 0.002674346509,'Fe' : 0.0002504387073, 'K': 0.0063684402277,'Mg' :0.00099225162461, 'Mn': 0.0001266581442,'Na' : 0.0029036363721, 'O': 3.3128876396,'P' : 0.00028174463078, 'Si': 1.6423635207,'Ti' : 0.0048814082984}, 'mol'),
       'GGBFS' : ({'Al' : 0.25068590354, 'C' : 0.026137740473, 'Ca': 0.7400381436, 'Fe': 0.0062268216656, 'H': 0.24594931045, 'K': 0.0066234897348, 'Mg':0.18043867977, 'Na': 0.0050333680703, 'O':2.568871619, 'S': 0.034432524341, 'Si': 0.54806878508, 'Ti':0.0064682389664},'mol'), 
       'calcined_clay':({'Al' : 0.86001188208, 'Ca' : 0.0017850310446, 'Fe': 0.0037610756424, 'H': 0.16669147874, 'K': 0.0021253535161, 'Na':0.009690369309, 'O': 3.1677258055, 'P':0.0028208175955, 'S':0.0012502298115, 'Si':0.86631529281, 'Ti': 0.018797209003},'mol'), 
       'fly_ash': ({'Al' : 0.35503673551, 'C' : 0.016132843743, 'Ca': 0.17672003338, 'Fe': 0.11522454383, 'K': 0.051594547539, 'Mg':0.087831601512, 'Na': 0.03259168428, 'O':2.8800652865,'S': 0.0049959283184, 'Si':  0.91038757213},'mol')}


'''
Formula in %mass for 100g: 
- Limestone: CaO: 42.3, Al2O3: 2.7, Fe2O3: 2, SiO2: 12.4, Na2O: 0.5, MgO: 1.8, K2O: 0.6, CO2: 37.7

- Silica Fume: CaO: 0.14997, MgO: 0.039992, K2O: 0.29994, Na2O: 0.089982, TiO2: 0.38992, Mn2O3: 0.009998, P2O5: 0.019996, Al2O3: 0.29994, Fe2O3: 0.019996, SiO2: 98.68

- GGBFS: SiO2: 32.93, Al2O3: 12.78, Fe2O3: 0.49717, CaO: 41.499, MgO: 7.2724, SO3: 0.14623, Na2O: 0.15598, K2O: 0.31195, H2S: 1.1113, H2O: 1.628, TiO2: 0.51667, CO2: 1.1503

- calcined clay: SiO2: 52.052, Al2O3: 43.844, Fe2O3: 0.3003, CaO: 0.1001, SO3: 0.1001, Na2O: 0.3003, K2O: 0.1001, TiO2: 1.5015, P2O5: 0.2002, H2O: 1.5015 

- fly ash: Ca0: 9.91, Al2O3: 18.1, Fe2O2: 9.2, SiO2: 54.7, CO2: 0.71, MgO: 3.54, K2O: 2.43, Na2O: 1.01, SO2: 0.4, P2O5: 0, C: 0

'''



# dictionary for the 5PL model (values found in cemgem)
dic_5lp = {'GGBFS' : {'A':0, 'B':1, 'C':7, 'D':70, 'G':1},
           'calcined_clay' : {'A':0, 'B':1, 'C':5, 'D':50, 'G':1},
           'fly_ash' : {'A':0, 'B':0.7, 'C':85.1, 'D':60, 'G':1}
          }

def add_material_to_gemsk(gemsk, recipe):
    ''' Add materials from recipe to gemsk object '''
    all_species = recipe['clink_phases'].copy()
    all_species["H2O@"] = recipe['wc'] #* 100
    all_species["Gp"]=recipe['CSH2']
    for name in all_species.keys():
        all_species[name]*=1e-3
    gemsk.T = recipe['T'] + 273.15
    gemsk.add_multiple_species_amt(all_species,units = "kg")
    for component in recipe.keys(): # add SCM
        if component not in ['clink_phases', 'wc', 'CSH2', 'T', 'RH', 'fineness']:
            #print(component)
            assert component in SCM.keys()
            value = recipe[component] *1e-3   # we give the input recipe in g
            gemsk.add_amt_from_formula(SCM[component][0], value, units='kg')  

    gemsk.add_species_amt("O2",1e-6) # to reduce stiffness related to redox

def add_material_for_hydration_to_gemsk(gemsk, recipe):
    ''' Add materials from recipe to gemsk object '''
    all_species = recipe['clink_phases'].copy()
    all_species["H2O@"] = recipe['wc'] #* 100
    all_species["Gp"]=recipe['CSH2']
    for name in all_species.keys():
        all_species[name]*=1e-3
    gemsk.T = recipe['T'] + 273.15
    gemsk.add_multiple_species_amt(all_species,units = "kg")
    gemsk.add_species_amt("O2",1e-6) # to reduce stiffness related to redox

def process_gems_result(gemsk):
    ''' Function to process gem result once the equilibration is done '''
    gemsk.supress_phase('gas_gen')
    gems_vol_frac=gemsk.phase_volumes
    gems_phase_amounts = gemsk.phase_amounts
    gems_B = gemsk.b
    init_vol = gemsk.system_volume
    for key,val in gems_vol_frac.items():
        gems_vol_frac[key]/=init_vol
    return gems_vol_frac, gems_phase_amounts, gems_B
    
def final_hydration(recipe, fail = False):
    ''' Function to compute the final hydration from a recipe '''
    input_file = 'gems_files/CemHyds-dat.lst'
    gemsk = GEMS(input_file)
    del recipe['wc_ratio']
    add_material_to_gemsk(gemsk, recipe)

# all the suppress_phases are taken from cemgems: 
    gemsk.supress_phase('gas_gen')
    gemsk.supress_phase('ettringite-AlFe')
    gemsk.supress_phase('monosulph-AlFe')
    gemsk.supress_phase('SO4_OH_AFm')
    gemsk.supress_phase('Gibbsite')
    gemsk.supress_phase('Kaolinite')
    gemsk.supress_phase('Graphite')
    gemsk.supress_phase('Belite')
    gemsk.supress_phase('CA')
    gemsk.supress_phase('CA2')
    gemsk.supress_phase('C3AH6')
    gemsk.supress_phase('C4AsH16')
    gemsk.supress_phase('C3FS0_84H4_32')
    gemsk.supress_phase('C3FS1_34H3_32')
    gemsk.supress_phase('Dolomite-dis')
    gemsk.supress_phase('thaumasite')
    gemsk.supress_phase('Hematite')
    gemsk.supress_phase('Goethite')
    gemsk.supress_phase('Pyrite')
    gemsk.supress_phase('Magnesite')
    gemsk.supress_phase('Pyrolusite')
    gemsk.supress_phase('Natrolite')
    gemsk.supress_phase('Quartz')


    
    gemsk.add_species_amt("O2@",0.001, units = 'kg')
    equilibration = gemsk.equilibrate()
    if equilibration[0] == 'F' or equilibration[0] == 'B':
        if fail == True:
            print('--- Equilibration Failed ---')
            print('--- Trying Again ---')
            gemsk.add_species_amt("H2O@",0.1, units = 'kg')
            equilibration = gemsk.equilibrate()
            if equilibration[0] == 'F' or equilibration[0] == 'B':
                print('Fail to equilibrate for recipe: ', recipe)
                return None
            else:
                print('--- Equilibration OK ---')
                return process_gems_result(gemsk)
                
    else:
        return process_gems_result(gemsk)

    
def alpha(t: float, SCM_type):
    ''' Compute the alpha in 5PL model in https://cemgems.org/tutorial/enhanced/redefining-processes/#b-the-5pl-4pl-logistic-function-model
        Used for the hydration of SCM (only implemented for GGBFS, Calcined Clay and Fly Ash)
    '''
    constant = dic_5lp[SCM_type]
    alpha = constant['D'] + (constant['A']-constant['D'])/(1+(t/constant['C'])**constant['B'])**constant['G']
    return alpha


def hydration(recipe, fail = False):
    ''' Function to compute the whole hydration from a recipe 
         Note that this model considers that all of the SCM reacts instanously
         and that only a portion of the clinker does react at each time point '''
    
    output_times = [0.,0.1,1,3,7,28,30,60,90,365, 730,1000,2000] #timesteps at which the model is evaluated in days (change it here and in the run.hydration file, they should be the same
    #use the p-k model to asses how much of each phase is available for hydration at each time step
    pk = parrot_killoh(recipe['wc_ratio'], recipe['RH'], recipe['T'], recipe['fineness']) 
    
    del recipe['wc_ratio'] #suppress the wc_ratio as this is not necessary from now on
    
    #initiate the GEMS class
    input_file = 'gems_files/CemHyds-dat.lst'
    gemsk = GEMS(input_file)
    
    #add the recipe in the gemsk object
    add_material_for_hydration_to_gemsk(gemsk, recipe)
    
    #do the hydration process
    clink_phases = recipe['clink_phases']
    gems_vol_frac = {}
    for i in range(len(output_times)):
        input_file = 'gems_files/CemHyds-dat.lst'
        gemsk = GEMS(input_file)
    
        #add the recipe in the gemsk object
        add_material_for_hydration_to_gemsk(gemsk, recipe)
        if i > 0: gemsk.warm_start()
        for phase in clink_phases:
           #set lower bound to limit dissolution for unreacted phases
            gemsk.species_lower_bound(phase,clink_phases[phase]*(1-pk[phase][i])*1e-3,units="kg")
        #equilibrate the recipe
        #gemsk.species_lower_bound('GGBFS', recipe['GGBFS']*(1-alpha(output_times[i], 'GGBFS'))*1e-3,units="kg")
        for component in recipe.keys(): # add SCM
            if component not in ['clink_phases', 'wc', 'CSH2', 'T', 'RH', 'fineness']:
                assert component in SCM.keys()
                try:
                    value = recipe[component] *1e-3 * alpha(output_times[i], component) *1e-2
                except KeyError: # if the component is not in the dic_5pl
                    value = recipe[component] *1e-3
                if SCM[component][1] == 'mol':
                    gemsk.add_amt_from_formula(SCM[component][0], value, units='mol')
                elif SCM[component][1] == 'kg':
                    gemsk.add_amt_from_formula(SCM[component][0], value, units='kg')
                else:
                    raise UnitsError ('the unit is not taken into account please select "mol" or "kg"')
        gemsk.supress_phase('gas_gen')
        equilibration = gemsk.equilibrate()
        
        #if equilibration fails
        if equilibration[0] == 'F' or equilibration[0] == 'B': 
            #print('Equilibration fails for recipe: ', recipe, equilibration, i)
            return None
        #if equilibration succeed
        else: 
            #suppress the gas phase as it would have enormous volume
            gemsk.supress_phase('gas_gen')
            
            if output_times[i]==0: init_vol = gemsk.system_volume
            gems_vol_frac[output_times[i]]=gemsk.phase_volumes 
            for key,val in gems_vol_frac[output_times[i]].items():
                gems_vol_frac[output_times[i]][key]/=init_vol
    return gems_vol_frac