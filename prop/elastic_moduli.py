import pandas as pd
import numpy as np
from scipy.optimize import fsolve


#vol_phase ={'CSH' : 0.4,
#            'C3S' : None,
#            'C2S' : None,
#            'C3A' : None,
#            'C4AF': None,
#            'CH' : 0.3, #portlandite
#            'C3AH6' : 0.1,#ettringite
#            'Water' : 0.3,         
#    }
bulk_moduli_phase = {'CSHQ' : 14.9, # obtain in Modeling the elastic properties of Portland cement paste Haecker+al.
            'Belite': 105.2,
            'Alite' : 105.2,
            'Aluminate' : 105.2,
            'Ferrite': 105.2,
            'Portlandite' : 40.0,
            'C3AH6' :14.9,
            'C4AsH12': 40.0,
            'aq_gen' : 2.2,
            'SiO2': 36.5,
            'ettringite-AlFe':14.9,
            "hemihydrate": 52.4,
            "Anhydrite": 54.9, 
            "arcanite": 31.9, 
            "thenardite": 43.4, 
            "Limestone": 69.8, 
            "monosulph-AlFe": 40.0, 
            'OH-hydrotalcite': 14.9, 
            'other_material': 14.9 # default value as CSH 14.9, double check with litterature,                    
    }
shear_moduli_phase = {'CSHQ' : 9.0, # obtain in Modeling the elastic properties of Portland cement paste Haecker+al.
            'Belite' : 44.8,
            'Alite' : 44.8,
            'Aluminate' : 44.8,
            'Ferrite': 44.8,
            'Portlandite' : 16.0,
            'C3AH6' :9.0,
            'C4AsH12': 16.0,
            'aq_gen' : 0.0,
            'SiO2': 31.2,
            'ettringite-AlFe':9.0,
            "hemihydrate": 24.2,
            "Anhydrite": 29.3, 
            "arcanite": 17.4, 
            "thenardite": 22.3, 
            "Limestone": 30.4, 
            "monosulph-AlFe": 16.0, 
            'OH-hydrotalcite': 9, 
            'other_material': 9.0 # default value as CSH double check with litterature 
                     }
def create_vol_phase(index, data):
    ''' create the dictionary vol_phase from data '''
    vol_phase_dic = {}
    for i in range(index.shape[0]):
        vol_phase_dic[index[i]] = data.iloc[i]
        #print(index[i], data.iloc[i])
    vol_phase = vol_phase_dic
    vol_phase['C3S'] = None #we consider that their are negligable for the elastic moduli at the end of hydration
    vol_phase['C2S'] = None
    vol_phase['C3A'] = None
    vol_phase['C4AF'] = None
    return vol_phase
def eq_sys(x):
    ''' Function to compute homogenized bulk and shear modulus using self consistent scheme according to Bernard + all'''
    nominator_bulk = 0
    nominator_shear = 0
    denominator_bulk = 0
    denominator_shear = 0
    for key,value in vol_phase.items():
        if value == None:
            #print('No value given for',key,' phase')
            continue
        else:
            nominator_bulk += value*bulk_moduli_phase[key]*(1+(3*x[0]/(3*x[0]+4*x[1]))*(bulk_moduli_phase[key]/x[0]-1))
            denominator_bulk += value*(1+(3*x[0]/(3*x[0]+4*x[1]))*(bulk_moduli_phase[key]/x[0]-1))
            
            nominator_shear += value*shear_moduli_phase[key]*(1+((6/5)*(x[0]+2*x[1])/(3*x[0]+4*x[1]))*(shear_moduli_phase[key]/x[1]-1))
            denominator_shear += value*(1+((6/5)*(x[0]+2*x[1])/(3*x[0]+4*x[1]))*(shear_moduli_phase[key]/x[1]-1))
    
    assert denominator_shear != 0
    assert denominator_bulk != 0
    
    return [x[0]-(nominator_bulk/denominator_bulk), x[1]- (nominator_shear/denominator_shear)]

#def create_dic_from_vol_phase(vol_phase : 

def elastic_moduli():
    ''' Compute the elastic moduli based on volume phases values
        Implemantation of https://www.sciencedirect.com/science/article/pii/S0008884603000395
    '''
    #create_dic_from_vol_phase(vol_phase)
    
    root = fsolve(eq_sys, [0.1, 0.1])
    k_hom = root[0]
    g_hom = root[1]

    y_hom = 9*1/(1/k_hom + 3/g_hom)
    p_hom = (3*k_hom - 2*g_hom)/(2*(3*k_hom + g_hom))
    return k_hom, g_hom, y_hom, p_hom

def data_elastic_moduli(data):
    ''' Create an elastic moduli dataset '''
    global vol_phase
    
    elastic_moduli_np=np.zeros((data.shape[1], 4))
    index = data.index
    for i in range(data.shape[1]):
        vol_phase = create_vol_phase(index, data.iloc[:,i])
        elastic_moduli_np[i,:] = elastic_moduli() #k_hom, g_hom, y_hom, p_hom
    return pd.DataFrame(elastic_moduli_np, columns=['K', 'G', 'E', 'P'])

def elastic_moduli_1d(data):
    ''' Create an elastic moduli dataset '''
    global vol_phase
    index = data.index
    print(index)
    vol_phase = create_vol_phase(index, data)
    elastic_moduli_np = elastic_moduli() #k_hom, g_hom, y_hom, p_hom
    return elastic_moduli_np[1] #G

